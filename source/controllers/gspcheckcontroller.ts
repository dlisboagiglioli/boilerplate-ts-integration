import { Controller, Get } from "routing-controllers";
import { IServiceResult, ServiceResultStatus } from "../commons/serviceresult";

@Controller("/gspcheck")
export default class GSPCheckController {

    @Get("/v1")
    public v1(
    ): IServiceResult {
        const result: IServiceResult = {
            status: ServiceResultStatus.Success,
            message: "Success"
        };
        return result;
    }

}