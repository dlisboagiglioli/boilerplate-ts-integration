import { Controller, Get, Post } from "routing-controllers";
import { IServiceResult, ServiceResultStatus } from "../commons/serviceresult";
import MockListService from "../services/mocklistservice";

@Controller("/mocklist")
export default class MockListController {
    constructor(
        private mock_listService: MockListService
    ) {
    }

    @Get("/v1")
    public async v1(
    ): Promise<IServiceResult> {
        const result: IServiceResult = {
            status: ServiceResultStatus.Success,
            message: "Success1",
            model: await this.mock_listService.v1()
        };
        return result;
    }

    @Post("/v1")
    public async create(
    ): Promise<IServiceResult> {
        const res: IServiceResult = {
            status: ServiceResultStatus.Success,
            message: "HAHAHAHA",
            model: await this.mock_listService.create()
        };
        return res;
    }

}