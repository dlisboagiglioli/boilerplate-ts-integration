import { Service } from "typedi";
import IMock from "../models/imock";

@Service()
export default class MockRepository {
    public list(): Promise<IMock[]> {
        return new Promise((resolve, reject) => {
            const list: IMock[] = [
                {
                    id: 1,
                    name: "Dfasdfasdo"
                },
                {
                    id: 2,
                    name: "Douglas"
                },
                {
                    id: 3,
                    name: "Thiago"
                },
            ];
            resolve(list);
        });
    }

    public create(): Promise<IMock[]> {
        return new Promise((resolve, reject) => {
            const create: IMock[] = [
                {
                    id: 1,
                    name: "SADASDA"
                },
                {
                    id: 2,
                    name: "DougDSADSAlas"
                },
                {
                    id: 3,
                    name: "DADAS"
                },
            ];
            resolve(create);
        });
    }
}