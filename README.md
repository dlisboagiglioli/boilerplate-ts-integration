## Rodar os codigos e build
```sh
npm install
npm run dev
npm run lint
npm run build
npm start
```

## Rodar os testes
```sh
npm run test
```

## Rodar a imagem do docker
```sh
npm run prod
```
